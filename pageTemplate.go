package yatwiki

import (
	"fmt"
	"html/template"
	"time"
)

var subresourceNonce = time.Now().Unix()

type pageTemplateOptions struct {
	CurrentPageIsArticle bool
	CurrentPageName      string
	CurrentPageIsRev     bool
	CurrentPageRev       int64
	WikiTitle            string
	Content              template.HTML
	BaseURL              string
	LoadCodeResources    bool
	DefaultPage          string
	AllowDownload        bool
	SessionMessage       string
	PageNotExistsError   bool
	PageNotExistsTarget  string
}

func DefaultPageTemplateOptions(opts *ServerOptions) *pageTemplateOptions {
	return &pageTemplateOptions{
		WikiTitle:     opts.PageTitle,
		BaseURL:       opts.ExpectBaseURL,
		DefaultPage:   opts.DefaultPage,
		AllowDownload: opts.AllowDBDownload,
	}
}

func (this *pageTemplateOptions) NewArticleTitle() string {
	return fmt.Sprintf("untitled-%d", time.Now().Unix())
}

func (this *pageTemplateOptions) SubresourceNonce() int64 {
	return subresourceNonce
}

const pageTemplate string = `<!DOCTYPE html>
<html>
	<head>
		<title>{{.CurrentPageName}}{{ if len .CurrentPageName }} - {{end}}{{.WikiTitle}}</title>		
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
		<meta name="referrer" content="no-referrer">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="alternate" type="application/rss+xml" href="{{.BaseURL}}rss/changes" title="{{.WikiTitle}} - Recent Changes">
		<link rel="stylesheet" type="text/css" href="{{.BaseURL}}wiki.css?_={{.SubresourceNonce}}">
{{if .LoadCodeResources}}
		<script type="text/javascript" src="{{.BaseURL}}highlight.js?_={{.SubresourceNonce}}"></script>
		<script type="text/javascript">hljs.tabReplace = '    ';hljs.initHighlightingOnLoad();</script>
{{end}}
		<script type="text/javascript">
var a = document;
function tid(id) { 
	t(a.getElementById(id));
}
function ts(e) {
	t(e.nextSibling);
}
function t(e) {
	e.style.display=(e.style.display=='none') ? 'block' : 'none';
}
function els(e,s){ // no js exec in innerHTML 
	var p = e.parentNode;
	p.className = "";
	p.innerHTML = s;
	var n = "script";
	var z = p.childNodes,
	    m = "text/javascript",
	    l = function(s){ return s.toLowerCase(); };
	for (var i=0, e=0; e = z[i]; i++) {
 		if (e.nodeName && (l(e.nodeName)===n) && (!e.type||l(e.type)===m)) {
			var t = p.removeChild(e),
			    d = a.getElementsByTagName("head")[0],
			    se = a.createElement(n);
			se.type = m;
			se.appendChild(a.createTextNode((t.text||t.textContent||t.innerHTML||"")));
			d.insertBefore(se,d.firstChild );
		}
	}
}
		</script>
	</head>
	<body>
		<div class="header">
			<a href="{{.BaseURL}}view/{{.DefaultPage | pathcomponent}}" title="Home"><div class="sprite">
				<svg viewBox="0 0 24 24">
    				<path d="M10,20V14H14V20H19V12H22L12,3L2,12H5V20H10Z" />
				</svg>
			</div></a>
			<a href="javascript:;" onclick="tid('spm');tid('tr1');tid('tr2');" title="Menu"><div class="sprite">
				<svg viewBox="0 0 24 24">
    				<path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z" />
				</svg>
			</div></a>
			<a href="{{.BaseURL}}modify/{{.NewArticleTitle | pathcomponent}}" title="New Page"><div class="sprite">
				<svg viewBox="0 0 24 24">
				    <path d="M13,9V3.5L18.5,9M6,2C4.89,2 4,2.89 4,4V20A2,2 0 0,0 6,22H18A2,2 0 0,0 20,20V8L14,2H6Z" />
				</svg>
			</div></a>
{{if .CurrentPageIsArticle }}
			<div class="sep"></div>
			<a href="{{.BaseURL}}history/{{.CurrentPageName | pathcomponent}}" title="Page History"><div class="sprite">
				<svg viewBox="0 0 24 24">
				    <path d="M15,13H16.5V15.82L18.94,17.23L18.19,18.53L15,16.69V13M19,8H5V19H9.67C9.24,18.09 9,17.07 9,16A7,7 0 0,1 16,9C17.07,9 18.09,9.24 19,9.67V8M5,21C3.89,21 3,20.1 3,19V5C3,3.89 3.89,3 5,3H6V1H8V3H16V1H18V3H19A2,2 0 0,1 21,5V11.1C22.24,12.36 23,14.09 23,16A7,7 0 0,1 16,23C14.09,23 12.36,22.24 11.1,21H5M16,11.15A4.85,4.85 0 0,0 11.15,16C11.15,18.68 13.32,20.85 16,20.85A4.85,4.85 0 0,0 20.85,16C20.85,13.32 18.68,11.15 16,11.15Z" />
				</svg>
			</div></a>
			<a href="{{.BaseURL}}modify/{{.CurrentPageName | pathcomponent}}" title="Modify Page"><div class="sprite">
				<svg viewBox="0 0 24 24">
				    <path d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
				</svg>
			</div></a>
	{{if .CurrentPageIsRev }}
			<a href="{{.BaseURL}}raw/{{.CurrentPageRev}}" title="Page Source"><div class="sprite">
				<svg viewBox="0 0 24 24">
				    <path d="M8,3A2,2 0 0,0 6,5V9A2,2 0 0,1 4,11H3V13H4A2,2 0 0,1 6,15V19A2,2 0 0,0 8,21H10V19H8V14A2,2 0 0,0 6,12A2,2 0 0,0 8,10V5H10V3M16,3A2,2 0 0,1 18,5V9A2,2 0 0,0 20,11H21V13H20A2,2 0 0,0 18,15V19A2,2 0 0,1 16,21H14V19H16V14A2,2 0 0,1 18,12A2,2 0 0,1 16,10V5H14V3H16Z" />
				</svg>
			</div></a>
	{{end}}
{{end}}
		</div>
		<div id="tr1" style="display:none;"></div>
		<div id="tr2" style="display:none;"></div>
		<div class="ddmenu" id="spm" style="display:none;">
			<a href="{{.BaseURL}}recent/1"><div class="sprite"></div> Recent Changes</a>
			<a href="{{.BaseURL}}random"><div class="sprite">
				<svg viewBox="0 0 24 24">
				    <path d="M12,18A6,6 0 0,1 6,12C6,11 6.25,10.03 6.7,9.2L5.24,7.74C4.46,8.97 4,10.43 4,12A8,8 0 0,0 12,20V23L16,19L12,15M12,4V1L8,5L12,9V6A6,6 0 0,1 18,12C18,13 17.75,13.97 17.3,14.8L18.76,16.26C19.54,15.03 20,13.57 20,12A8,8 0 0,0 12,4Z" />
				</svg>
			</div> Random Page</a>
			<a href="{{.BaseURL}}index"><div class="sprite"></div> Article Index</a>
{{if .AllowDownload}}
			<a href="{{.BaseURL}}download-database" download><div class="sprite"></div> Download DB backup</a>
{{end}}
		</div>
		<div class="content">
{{if .PageNotExistsError}}
			<div class="info">
				No such article exists.
				<a href="{{.BaseURL}}modify/{{.PageNotExistsTarget | pathcomponent}}">Click here</a> to create it.
			</div>
{{end}}
{{if len .SessionMessage}}
			<div class="info">{{.SessionMessage}}</div>
{{end}}
{{.Content}}
		</div>
	</body>
</html>`
