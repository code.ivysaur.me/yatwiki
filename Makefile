#
# Makefile for YATWiki3
#

VERSION:=3.3.1

SOURCES:=Makefile \
	static \
	cmd $(wildcard cmd/yatwiki-server/*.go) \
	Gopkg.lock Gopkg.toml \
	$(wildcard *.go)

GOFLAGS:=-a \
	-ldflags "-s -w -X code.ivysaur.me/yatwiki.SERVER_HEADER=YATWiki/$(VERSION)" \
	-gcflags '-trimpath=$(GOPATH)' \
	-asmflags '-trimpath=$(GOPATH)'

#
# Phony targets
#
	
.PHONY: all dist clean deps

all: build/linux64/yatwiki-server build/win32/yatwiki-server.exe

dist: \
	_dist/yatwiki-$(VERSION)-linux64.tar.gz \
	_dist/yatwiki-$(VERSION)-win32.7z \
	_dist/yatwiki-$(VERSION)-src.zip

clean:
	rm -f ./staticResources.go
	rm -fr ./build
	rm -f ./yatwiki
	
deps:
	go get -u github.com/jteeuwen/go-bindata/...
	go get -u github.com/golang/dep/cmd/dep
	dep ensure

#
# Generated files
#

staticResources.go: static/ static/*
	go-bindata -o staticResources.go -prefix static -pkg yatwiki static
	

#
# Release artefacts
#
	
build/linux64/yatwiki-server: $(SOURCES) staticResources.go
	mkdir -p build/linux64
	(cd cmd/yatwiki-server ; \
		CGO_ENABLED=1 GOOS=linux GOARCH=amd64 \
		go build $(GOFLAGS) -o ../../build/linux64/yatwiki-server \
	)
	
build/win32/yatwiki-server.exe: $(SOURCES) staticResources.go
	mkdir -p build/win32
	(cd cmd/yatwiki-server ; \
		PATH=/usr/lib/mxe/usr/bin:$(PATH) CC=i686-w64-mingw32.static-gcc \
		CGO_ENABLED=1 GOOS=windows GOARCH=386 \
		go build $(GOFLAGS) -o ../../build/win32/yatwiki-server.exe \
	)

_dist/yatwiki-$(VERSION)-linux64.tar.gz: build/linux64/yatwiki-server
	mkdir -p _dist
	tar caf _dist/yatwiki-$(VERSION)-linux64.tar.gz -C build/linux64 yatwiki-server --owner=0 --group=0
	
_dist/yatwiki-$(VERSION)-win32.7z: build/win32/yatwiki-server.exe
	mkdir -p _dist
	( cd build/win32 ; \
		if [ -f dist.7z ] ; then rm dist.7z ; fi ; \
		7z a dist.7z yatwiki-server.exe ; \
		mv dist.7z ../../_dist/yatwiki-$(VERSION)-win32.7z \
	)
	
_dist/yatwiki-$(VERSION)-src.zip: $(SOURCES)
	 git archive --format=zip HEAD > _dist/yatwiki-$(VERSION)-src.zip
	
