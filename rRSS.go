package yatwiki

import (
	"fmt"
	"html/template"
	"net/http"
	"net/url"
	"time"
)

func (this *WikiServer) routeRecentChangesRSS(w http.ResponseWriter, r *http.Request) {
	recents, err := this.db.GetRecentChanges(0, this.opts.RecentChangesRSS)
	if err != nil {
		this.serveInternalError(w, r, err)
		return
	}

	content := `<?xml version="1.0" encoding="UTF-8" ?> 
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom"> 
	<channel>
		<title>` + template.HTMLEscapeString(this.opts.PageTitle) + `</title>
		<link>` + template.HTMLEscapeString(this.opts.ExternalBaseURL) + `</link>
		<description>` + template.HTMLEscapeString(this.opts.PageTitle) + `</description>
		<language>` + template.HTMLEscapeString(this.opts.DeclareRSSLanguage) + `</language>
		<atom:link href="` + template.HTMLEscapeString(this.opts.ExternalBaseURL+`rss/changes`) + `" rel="self" type="application/rss+xml" />
`
	for _, a := range recents {
		content += `
		<item>
			<title>` + template.HTMLEscapeString(a.Title+` (r`+fmt.Sprintf("%d", a.ArticleID)+`)`) + `</title>
			<link>` + template.HTMLEscapeString(this.opts.ExternalBaseURL+`archive/`+fmt.Sprintf("%d", a.ArticleID)) + `</link>
			<guid>` + template.HTMLEscapeString(this.opts.ExternalBaseURL+`archive/`+fmt.Sprintf("%d", a.ArticleID)) + `</guid>
			<author>` + template.HTMLEscapeString(this.opts.DeclareRSSEmail+` (`+this.opts.PageTitle+` `+a.Author+`)`) + `</author>
			<pubDate>` + template.HTMLEscapeString(time.Unix(a.Modified, 0).In(this.loc).Format(time.RFC1123Z)) + `</pubDate>
			<description>` + template.HTMLEscapeString(`
				<a href="`+template.HTMLEscapeString(this.opts.ExternalBaseURL+`view/`+url.PathEscape(a.Title))+`">latest version</a>
				|
				<a href="`+template.HTMLEscapeString(this.opts.ExternalBaseURL+`archive/`+fmt.Sprintf("%d", a.ArticleID))+`">revision `+fmt.Sprintf("%d", a.ArticleID)+`</a>
				|
				<a href="`+template.HTMLEscapeString(this.opts.ExternalBaseURL+`diff/parent/`+fmt.Sprintf("%d", a.ArticleID))+`">diff to previous</a>
			`) + `</description>
		</item>
		`
	}
	content += `
	</channel>
</rss>`

	w.Header().Set(`Content-Type`, `application/rss+xml`)
	w.Write([]byte(content))
}
