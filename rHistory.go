package yatwiki

import (
	"database/sql"
	"fmt"
	"html/template"
	"net/http"
	"net/url"
)

func (this *WikiServer) routeHistory(w http.ResponseWriter, r *http.Request, articleTitle string) {
	revs, err := this.db.GetRevisionHistory(articleTitle)
	if err != nil {
		if err == sql.ErrNoRows {
			this.serveNoSuchArticle(w, articleTitle)
			return
		}

		this.serveErrorMessage(w, err)
		return
	}

	if len(revs) == 0 {
		this.serveNoSuchArticle(w, articleTitle)
		return
	}

	pto := DefaultPageTemplateOptions(this.opts)
	pto.CurrentPageName = articleTitle
	pto.CurrentPageIsArticle = true

	content := `<h2>Page History</h2><br>` +
		`<em>There have been ` + fmt.Sprintf("%d", len(revs)) + ` edits to the page &quot;<a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`view/`+url.PathEscape(articleTitle)) + `">` + template.HTMLEscapeString(articleTitle) + `</a>&quot;.</em>` +
		`<br><br>` +
		`<form method="GET" action="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`diff`) + `">` +
		`<table>`
	compareRow := `<tr><td colspan="2"></td><td><input type="submit" value="Compare Selected &raquo;"></td></tr>`
	content += compareRow
	for _, rev := range revs {
		revIdStr := fmt.Sprintf("%d", rev.ArticleID)
		content += `<tr>` +
			`<td><a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`archive/`+revIdStr) + `">` + string(this.formatTimestamp(rev.Modified)) + `</a></td>` +
			`<td>` + template.HTMLEscapeString(rev.Author) + `</td>` +
			`<td><input type="radio" name="t" value="` + revIdStr + `">&nbsp;<input type="radio" name="f" value="` + revIdStr + `"></td>` +
			`</tr>`
	}
	content += compareRow
	content += `</table></form>`

	pto.Content = template.HTML(content)
	this.servePageResponse(w, r, pto)
	return
}
