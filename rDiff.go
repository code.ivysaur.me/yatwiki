package yatwiki

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"html/template"
	"net/http"

	"code.ivysaur.me/yatwiki/diff"
)

func (this *WikiServer) routeDiff(w http.ResponseWriter, r *http.Request, oldRev, newRev int) {
	if oldRev > newRev {
		oldRev, newRev = newRev, oldRev
	}

	oa, err := this.db.GetRevision(oldRev)
	if err != nil {
		if err == sql.ErrNoRows {
			this.serveErrorMessage(w, errors.New("Invalid revision selected"))
			return
		}

		this.serveErrorMessage(w, err)
		return
	}

	na, err := this.db.GetRevision(newRev)
	if err != nil {
		if err == sql.ErrNoRows {
			this.serveErrorMessage(w, errors.New("Invalid revision selected"))
			return
		}

		this.serveErrorMessage(w, err)
		return
	}

	if oa.TitleID != na.TitleID {
		this.serveErrorMessage(w, fmt.Errorf("Preventing attempt to compare mismatched articles (%s ≠ %s)", oa.Title, na.Title))
		return
	}

	b := bytes.Buffer{}
	diff.HtmlDiff(string(oa.Body), string(na.Body), &b)

	pto := DefaultPageTemplateOptions(this.opts)
	pto.CurrentPageName = oa.Title
	pto.CurrentPageIsArticle = true

	pto.Content = template.HTML(
		`<h2>` +
			`Comparing ` + string(this.viewLink(oa.Title)) + ` versions ` +
			`<a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`archive/`+fmt.Sprintf("%d", oa.ArticleID)) + `">r` + fmt.Sprintf("%d", oa.ArticleID) + `</a>` +
			` - ` +
			`<a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`archive/`+fmt.Sprintf("%d", na.ArticleID)) + `">r` + fmt.Sprintf("%d", na.ArticleID) + `</a>` +
			`</h2>` +
			`<pre>` + string(b.Bytes()) + `</pre>`,
	)
	this.servePageResponse(w, r, pto)
	return
}
