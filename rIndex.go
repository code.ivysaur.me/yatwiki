package yatwiki

import (
	"fmt"
	"html/template"
	"net/http"
	"net/url"
)

func (this *WikiServer) routeIndex(w http.ResponseWriter, r *http.Request) {
	titles, err := this.db.ListTitles(true) // Always load deleted pages, even if we don't display them in the list
	if err != nil {
		this.serveInternalError(w, r, err)
		return
	}

	showDeleted := (r.FormValue("deleted") == "1")
	anyDeleted := false

	totalRevs, err := this.db.TotalRevisions()
	if err != nil {
		this.serveInternalError(w, r, err)
		return
	}

	content := fmt.Sprintf(`<h2>Article Index</h2><br><em>There are %d edits to %d pages.</em><br><br><ul>`, totalRevs, len(titles))
	for _, title := range titles {
		classAttr := ""
		if title.IsDeleted {
			anyDeleted = true
			if !showDeleted {
				continue
			}
			classAttr = `class="deleted"`
		}
		content += `<li><a ` + classAttr + ` href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`view/`+url.PathEscape(title.Title)) + `">` + template.HTMLEscapeString(title.Title) + `</a></li>`
	}
	content += `</ul>`

	if anyDeleted {
		content += `<br>`
		if !showDeleted {
			content += `<a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`index?deleted=1`) + `">Show deleted pages</a>`
		} else {
			content += `<a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`index`) + `">Hide deleted pages</a>`
		}
	}

	pto := DefaultPageTemplateOptions(this.opts)
	pto.CurrentPageName = "Index"
	pto.Content = template.HTML(content)
	this.servePageResponse(w, r, pto)
	return
}
