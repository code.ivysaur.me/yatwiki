package yatwiki

import (
	"html/template"
	"net/http"
)

func (this *WikiServer) routeFormatting(w http.ResponseWriter, r *http.Request) {
	pto := DefaultPageTemplateOptions(this.opts)
	pto.CurrentPageName = "Formatting help"

	content := `
<h2>Formatting help</h2><br><br>
<ul>
	<li>[h]header[/h]</li>
	<li>[b]bold[/b]</li>
	<li>[u]underline[/u]</li>
	<li>[i]italic[/i]</li>
	<li>[s]strikethrough[/s]</li>
	<li>[spoiler]spoiler[/spoiler]</li>
	<li>[list] item [*] item [/list]</li>
	<li>[url=address]title[/url]</li>
	<li>[article=page name]title[/article] or [rev=id]title[/rev]</li>
	<li>[img]image-url[/img]</li>
	<li>[imgur]asdf.jpg[/imgur]</li>
`
	if len(this.opts.ContentedBBCodeTag) > 0 {
		content += `
	<li>[` + this.opts.ContentedBBCodeTag + `]abc[/` + this.opts.ContentedBBCodeTag + `]</li>
		`
	}

	content += `
	<li>[code]fixed width[/code]</li>
	<li>[section=header]content[/section]</li>
	<li>[youtube]id[/youtube]</li>
	<li>[html]raw html[/html]</li>
	<li>` + bbcodeIsDeleted + `</li>
</ul>`

	pto.Content = template.HTML(content)

	this.servePageResponse(w, r, pto)
}
