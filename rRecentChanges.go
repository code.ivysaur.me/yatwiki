package yatwiki

import (
	"errors"
	"fmt"
	"html/template"
	"math"
	"net/http"
	"net/url"
)

func (this *WikiServer) routeRecentChanges(w http.ResponseWriter, r *http.Request, pageNum int) {

	totalEdits, err := this.db.TotalRevisions()
	if err != nil {
		this.serveInternalError(w, r, err)
		return
	}

	minPage := 1
	maxPage := int(math.Ceil(float64(totalEdits) / float64(this.opts.RecentChanges)))

	if pageNum < minPage || pageNum > maxPage {
		this.serveErrorMessage(w, errors.New("Invalid page."))
		return
	}

	recents, err := this.db.GetRecentChanges((pageNum-1)*this.opts.RecentChanges, this.opts.RecentChanges)
	if err != nil {
		this.serveErrorMessage(w, err)
		return
	}

	//

	pto := DefaultPageTemplateOptions(this.opts)
	pto.CurrentPageName = "Recent Changes"

	content := `<h2>Recent Changes</h2><br>` +
		`<em>Showing up to ` + fmt.Sprintf("%d", this.opts.RecentChanges) + ` changes.</em><br><br>` +
		`<div style="display:inline-block;">` +
		`<table class="ti">` +
		`<tr><td>Page</td><td>Actions</td><td>Time</td><td>Author</td></tr>`
	for _, rev := range recents {

		diffHtml := ""
		diffRev, err := this.db.GetNextOldestRevision(int(rev.ArticleID))
		if err != nil {
			diffHtml = `[new]`
		} else {
			diffHtml = `<a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`diff/`+fmt.Sprintf("%d/%d", diffRev, rev.ArticleID)) + `">diff</a>`
		}

		classAttr := ""
		if rev.IsDeleted {
			classAttr = `class="deleted"`
		}

		content += `<tr>` +
			`<td><a ` + classAttr + ` href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`view/`+url.PathEscape(rev.Title)) + `">` + template.HTMLEscapeString(rev.Title) + `</a></td>` +
			`<td>` +
			`<a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`archive/`+fmt.Sprintf("%d", rev.ArticleID)) + `">rev</a> &nbsp; ` +
			diffHtml +
			`</td>` +
			`</td>` +
			`<td>` + string(this.formatTimestamp(rev.Modified)) + `</td>` +
			`<td>` + template.HTMLEscapeString(rev.Author) + `</td>` +
			`</tr>`
	}
	content += `</table>`

	if pageNum > 1 {
		content += `<span style="float:left;"><a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`recent/`+fmt.Sprintf("%d", pageNum-1)) + `">&laquo; Newer</a></span>`
	}

	if pageNum < maxPage {
		content += `<span style="float:right;"><a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`recent/`+fmt.Sprintf("%d", pageNum+1)) + `">Older &raquo;</a></span>`
	}
	content += `</div>`

	pto.Content = template.HTML(content)
	this.servePageResponse(w, r, pto)
	return
}
