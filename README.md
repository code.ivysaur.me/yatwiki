# YATWiki

A semi-anonymous wiki for use in trusted environments.

## Features

- Standalone server, easy to run
- Built-in SQLite database
- BBcode
- Diff page revisions
- RSS changelog
- IP-based ban system
- Article index, random article, download database backup
- Source code highlighting (thanks [highlight.js](https://github.com/isagalaev/highlight.js) )
- Optional integration with [`contented`](https://code.ivysaur.me/contented/) for file/image uploads

For the 20150901 release, a desktop version is available for Windows (based on PHPDesktop).

Prior to the 3.0 release, YATWiki was a PHP script instead of a standalone server.

## Usage

You can start YATWiki by running the binary. A default configuration file and database will be automatically generated if they are not found.

```
Usage of ./yatwiki-server:
  -config string
        Configuration file (default "config.json")
  -listen string
        Bind address (default "127.0.0.1:80")
```

## Changelog

2018-04-02 3.3.0
- Feature: Allow deleting (and un-deleting) articles
- Feature: Support `[youtube]` tag for embedded Youtube videos
- Feature: Add link to view raw page source when viewing a specific page revision
- Feature: Display timestamps on all console log messages
- Enhancement: Upgrade bundled SQLite3 library
- Disable zooming out on mobile devices (but zooming in is still allowed)
- Fix an issue with linking to articles where the name contained an apostrophe

2017-11-18 3.2.0
- Feature: Add new ContentedBBCodeTag option to choose a BBCode tag for mini thumbnails (requires `contented` >= 1.2.0)
- Feature: Replace menu image with SVG, for high-DPI screens
- Feature: Hover over timestamps to display in more detail
- Feature: Link to diff pages directly from the Recent Changes page
- Fix some cosmetic issues with the Recent Changes page

2017-10-29 3.1.3
- Fix one more case of article title normalisation

2017-10-29 3.1.2
- Lock dependency versions
- Enhancement: Advertise build number in Server headers
- Fix a regression in 3.x series with not normalising article titles
- Fix server response if favicon is not configured

2017-10-15 3.1.1
- Update `contented` integration (requires `contented` >= 1.1.0)

2017-10-08 3.1.0
- Feature: Support content upload to a `contented` server

2017-08-11 3.0.2
- Fix an issue with XSS prevention for web browsers other than Chrome

2017-08-11 3.0.1
- Feature: New `TrustXForwardedFor` config option for usage behind reverse proxies
- Fix an issue with article titles containing `+`
- Fix an issue with `[html]` tags
- Fix an issue with viewing history for unknown articles

2017-07-11 3.0.0
- YATWiki was rewritten in Go.
- Enhancement: Standalone binary server
- Enhancement: No longer requires cookies for error messages
- Enhancement: Add `norefer` attribute to outgoing links
- Improve performance of generating RSS content
- Removed support for ban-evasion codes and "download source code"

2017-06-24 r60
- Enhancement: Don't require `short_open_tag` in PHP configuration for new installs
- Fix a number of issues with handling of base URLs in links
- Fix a cosmetic issue with file caching for CSS content

2016-11-16 20161116
- (no public release)
- Enhancement: Always open the formatting help in a new tab
- Fix a cosmetic issue with display of backslash characters caused by Meiryo font

2016-08-24 20160824
- (no public release)
- Feature: Add Compare button to both top and bottom of article revision list
- Fix an issue with noncompliant HTML when comparing diffs

2015-09-01 r29
- Feature: Desktop version for Windows (based on PHPDesktop)
- Enhancement: Automatically determine whether HTTPS is in use, simplifying configuration
- Fix an issue with handling of base URLs in links
- Fix a cosmetic issue with document mode in Internet Explorer

2013-10-19 r20
- Enhancement: Include current-revision, diff-to-previous links inside RSS feed
- Enhancement: Change date display format
- Enhancement: Always diff in chronological order, regardless of selection order
- Fix a cosmetic issue with missing history/edit buttons when viewing an old page revision

2013-06-05 20130605
- Feature: Spoiler tags
- Feature: Raw HTML tags (the viewer must click through a warning to enable)
- Feature: IP bans
- Feature: RSS support
- Feature: Endpoint for accessing raw page content (`/raw/{pagename}`)
- Feature: Code highlighting via `highlight.js` (BSD license)
- Enhancement: Split change history across multiple pages
- Enhancement: Move formatting help to separate page
- Fix issues with blank-named pages, spaces in page names, and blank page content
- Fix some cosmetic issues on mobile browsers
- Fix a cosmetic issue when loading YATWiki with no database file

2012-03-24 20120324
- Merge into single-file
- Replace Markdown with BBCode formatting

2011-06-12 v1.1
- Replace flat-file article storage with SQLite DB

2011-05-03 v1.0
- Initial public release
- Flat-file article storage
- Markdown formatting
