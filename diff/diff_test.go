package diff_test

import (
	"reflect"
	"testing"

	"code.ivysaur.me/yatwiki/diff"
)

func TestDiff(t *testing.T) {

	type testCase struct {
		o, n   []string
		expect []diff.Instruction
	}

	testCases := []testCase{

		{
			[]string{},
			[]string{},
			[]diff.Instruction{},
		},
		{
			[]string{"a"},
			[]string{"b"},
			[]diff.Instruction{
				{diff.OP_REMOVED, []string{"a"}},
				{diff.OP_INSERTED, []string{"b"}},
			},
		},
		{
			[]string{"a", "b", "c"},
			[]string{"a", "b", "c", "d"},
			[]diff.Instruction{
				{diff.OP_UNCHANGED, []string{"a", "b", "c"}},
				{diff.OP_INSERTED, []string{"d"}},
			},
		},
		{
			[]string{"a", "b", "c", "d"},
			[]string{"a", "b", "X", "Y", "Z", "d"},
			[]diff.Instruction{
				{diff.OP_UNCHANGED, []string{"a", "b"}},
				{diff.OP_REMOVED, []string{"c"}},
				{diff.OP_INSERTED, []string{"X", "Y", "Z"}},
				{diff.OP_UNCHANGED, []string{"d"}},
			},
		},
	}

	for _, testCase := range testCases {
		output := diff.Diff(testCase.o, testCase.n)
		if !reflect.DeepEqual(output, testCase.expect) {
			t.Fail()
			t.Errorf("==EXPECTED==\n%#v\n==GOT==\n%#v\n", testCase.expect, output)
		}
	}

}
