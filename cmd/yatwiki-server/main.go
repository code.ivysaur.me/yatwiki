package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"code.ivysaur.me/yatwiki"
)

func main() {

	bindAddr := flag.String("listen", "127.0.0.1:80", "Bind address")
	configPath := flag.String("config", "config.json", "Configuration file")
	flag.Parse()

	opts := yatwiki.ServerOptions{}

	// Create default configuration file if necessary
	if _, err := os.Stat(*configPath); os.IsNotExist(err) {
		log.Printf("Creating default configuration file at '%s'...\n", *configPath)
		opts = *yatwiki.DefaultOptions()
		if cfg, err := json.MarshalIndent(opts, "", "\t"); err == nil {
			err := ioutil.WriteFile(*configPath, cfg, 0644)
			if err != nil {
				log.Printf("Failed to save default configuration file: %s", err.Error())
			}
		}
	}

	// Load configuration
	log.Printf("Loading configuration from '%s'...\n", *configPath)
	cfg, err := ioutil.ReadFile(*configPath)
	if err != nil {
		log.Fatalf("Failed to load configuration file '%s': %s\n", *configPath, err.Error())
	}
	err = json.Unmarshal(cfg, &opts)
	if err != nil {
		log.Fatalf("Failed to parse configuration file: %s\n", err.Error())
	}

	//

	ws, err := yatwiki.NewWikiServer(&opts)
	if err != nil {
		log.Fatalln(err.Error())
	}
	defer ws.Close()

	log.Printf("YATWiki now listening on %s\n", *bindAddr)
	err = http.ListenAndServe(*bindAddr, ws)
	if err != nil {
		log.Fatalln(err.Error())
	}
}
