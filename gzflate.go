package yatwiki

import (
	"bytes"
	"compress/flate"
	"io"
)

func gzinflate(gzBody []byte) ([]byte, error) {
	gzBodyReader := bytes.NewReader(gzBody)

	gzReader := flate.NewReader(gzBodyReader)
	defer gzReader.Close()

	buffer := bytes.Buffer{}
	_, err := io.Copy(&buffer, gzReader)
	if err != nil {
		return nil, err
	}

	return buffer.Bytes(), nil
}

func gzdeflate(plaintext []byte, level int) ([]byte, error) {
	compressedContent := bytes.Buffer{}

	zipper, err := flate.NewWriter(&compressedContent, level)
	if err != nil {
		return nil, err // e.g. bad level
	}
	defer zipper.Close()

	_, err = zipper.Write(plaintext)
	if err != nil {
		return nil, err
	}

	err = zipper.Close() // flush data
	if err != nil {
		return nil, err
	}

	return compressedContent.Bytes(), nil
}
