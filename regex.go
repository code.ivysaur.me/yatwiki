package yatwiki

import (
	"regexp"
)

func PregReplaceCallback(pattern *regexp.Regexp, cb func([]string) string, content string) string {
	// FIXME avoid double-matching
	// Submit to upstream https://github.com/golang/go/issues/5690
	return pattern.ReplaceAllStringFunc(content, func(fullmatch string) string {
		parts := pattern.FindStringSubmatch(fullmatch)
		return cb(parts)
	})

}
