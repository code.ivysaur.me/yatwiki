package yatwiki

import (
	"testing"
)

func TestBBCode(t *testing.T) {

	bbr := NewBBCodeRenderer("http://BASE_URL/", "http://CONTENTED_URL/", "[CONTENTED]")

	type testCase struct {
		input, expected string
	}

	testCases := []testCase{

		// Test basic functionality
		testCase{
			`identity`,
			`identity`,
		},

		// Expected youtube format
		testCase{
			`[youtube]dQw4w9WgXcQ[/youtube]`,
			`<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/dQw4w9WgXcQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>`,
		},
	}

	for _, tc := range testCases {

		// BBCode renderers are stateful
		bbr.Reset()

		output := bbr.RenderHTML(tc.input)
		if string(output) != tc.expected {
			t.Fatalf("Test failed: %s\nResult:\n%s\n", tc.input, output)
		}

	}
}
