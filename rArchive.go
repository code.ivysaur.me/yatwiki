package yatwiki

import (
	"database/sql"
	"errors"
	"html/template"
	"net/http"
	"net/url"
	"time"
)

func (this *WikiServer) routeArchive(w http.ResponseWriter, r *http.Request, revId int) {
	a, err := this.db.GetRevision(revId)
	if err != nil {
		if err == sql.ErrNoRows {
			this.serveErrorMessage(w, errors.New("No such revision."))
			return
		}

		this.serveErrorMessage(w, err)
		return
	}

	pto := DefaultPageTemplateOptions(this.opts)
	pto.CurrentPageName = a.Title
	pto.CurrentPageIsArticle = true

	pto.CurrentPageRev = int64(revId)
	pto.CurrentPageIsRev = true

	infoMessageHtml := `You are viewing a specific revision of this page, last modified ` +
		time.Unix(a.Modified, 0).In(this.loc).Format(this.opts.DateFormat) + `. `
	if !a.IsDeleted {
		infoMessageHtml += `Click <a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`view/`+url.PathEscape(a.Title)) + `">here</a> to see the latest revision.`
	}

	bcr := this.GetBBCodeRenderer()
	pto.Content = template.HTML(
		`<div class="info">`+infoMessageHtml+`</div>`,
	) + bcr.RenderHTML(string(a.Body))
	pto.LoadCodeResources = bcr.CodePresent

	this.servePageResponse(w, r, pto)
	return
}
