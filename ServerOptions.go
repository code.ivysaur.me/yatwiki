package yatwiki

import (
	"time"
)

type ServerOptions struct {
	PageTitle            string
	ExpectBaseURL        string
	DefaultPage          string
	Timezone             string
	DateFormat           string
	DBFilePath           string
	FaviconFilePath      string
	AllowDBDownload      bool
	TrustXForwardedFor   bool // Introduced in 3.0.1 - default false
	RecentChanges        int
	RecentChangesRSS     int
	GzipCompressionLevel int
	BannedUserIPRegexes  []string
	ExternalBaseURL      string
	DeclareRSSLanguage   string
	DeclareRSSEmail      string
	ContentedServer      string
	ContentedBBCodeTag   string
}

func DefaultOptions() *ServerOptions {
	return &ServerOptions{
		PageTitle:            "YATWiki",
		ExpectBaseURL:        "/",
		DefaultPage:          "home",
		Timezone:             "UTC",
		DateFormat:           time.RFC3339,
		DBFilePath:           "wiki.db",
		FaviconFilePath:      "", // no favicon
		AllowDBDownload:      true,
		TrustXForwardedFor:   false,
		RecentChanges:        20,
		RecentChangesRSS:     10,
		GzipCompressionLevel: 9,
		BannedUserIPRegexes:  make([]string, 0),
		ExternalBaseURL:      "http://127.0.0.1/",
		DeclareRSSLanguage:   "en-GB",
		DeclareRSSEmail:      `nobody@example.com`,
		ContentedServer:      "",
		ContentedBBCodeTag:   "",
	}
}
