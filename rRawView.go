package yatwiki

import (
	"database/sql"
	"errors"
	"net/http"
)

func (this *WikiServer) routeRawView(w http.ResponseWriter, r *http.Request, revId int) {
	a, err := this.db.GetRevision(revId)
	if err != nil {
		if err == sql.ErrNoRows {
			this.serveErrorMessage(w, errors.New("No such revision."))
			return
		}

		this.serveErrorMessage(w, err)
		return
	}

	w.Header().Set(`Content-Type`, `text/plain; charset=UTF-8`)
	w.WriteHeader(200)
	w.Write(a.Body)
	return
}
