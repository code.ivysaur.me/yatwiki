package yatwiki

import (
	"html/template"
	"log"
	"net/http"
	"net/url"
	"time"
)

func (this *WikiServer) serveErrorMessage(w http.ResponseWriter, err error) {
	this.serveErrorText(w, err.Error())
}

func (this *WikiServer) serveInternalError(w http.ResponseWriter, r *http.Request, e error) {
	log.Printf("Internal error from %s while accessing %s(%s): %s", r.RemoteAddr, r.Method, r.URL.Path, e.Error())
	http.Error(w, "An internal error occurred. Please ask an administrator to check the log file.", 500)
}

func (this *WikiServer) serveDeleted(w http.ResponseWriter, lookingFor string) {
	this.serveRedirect(w, this.opts.ExternalBaseURL+"history/"+url.PathEscape(lookingFor)+"?error="+url.QueryEscape(`The page you are looking for has been deleted. However, the history is still available.`))
}

func (this *WikiServer) serveErrorText(w http.ResponseWriter, msg string) {
	this.serveRedirect(w, this.opts.ExpectBaseURL+"view/"+url.PathEscape(this.opts.DefaultPage)+"?error="+url.QueryEscape(msg))
}

func (this *WikiServer) serveNoSuchArticle(w http.ResponseWriter, lookingFor string) {
	this.serveRedirect(w, this.opts.ExpectBaseURL+"view/"+url.PathEscape(this.opts.DefaultPage)+"?notfound="+url.QueryEscape(lookingFor))
}

func (this *WikiServer) serveRedirect(w http.ResponseWriter, location string) {
	w.Header().Set("Location", location)
	w.WriteHeader(302) // moved (not permanently)
}

func (this *WikiServer) servePageResponse(w http.ResponseWriter, r *http.Request, pto *pageTemplateOptions) {
	w.WriteHeader(200)

	if noSuchArticleTarget, ok := r.URL.Query()["notfound"]; ok {
		pto.PageNotExistsError = true
		pto.PageNotExistsTarget = noSuchArticleTarget[0]

	} else {
		pto.SessionMessage = r.URL.Query().Get("error")
	}

	err := this.pageTmp.Execute(w, pto)
	if err != nil {
		log.Println(err.Error())
	}
}

func (this *WikiServer) formatTimestamp(m int64) template.HTML {
	// TODO add a more detailed timestamp on hover
	dt := time.Unix(m, 0).In(this.loc)

	return template.HTML(`<span title="` + template.HTMLEscapeString(dt.Format(time.RFC3339)) + `">` + template.HTMLEscapeString(dt.Format(this.opts.DateFormat)) + `</span>`)
}

func (this *WikiServer) viewLink(articleTitle string) template.HTML {
	return template.HTML(`&quot;<a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`view/`+url.PathEscape(articleTitle)) + `">` + template.HTMLEscapeString(articleTitle) + `</a>&quot;`)
}
