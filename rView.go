package yatwiki

import (
	"database/sql"
	"net/http"
	"net/url"
)

func (this *WikiServer) routeView(w http.ResponseWriter, r *http.Request, articleTitle string) {
	a, err := this.db.GetLatestVersion(articleTitle)
	if err != nil {

		if err == sql.ErrNoRows {
			// Yatwiki2 always required a trailing slash at the end of the URL
			// If this was an old link, it might not be present.
			// Redirect if possible
			if len(articleTitle) > 0 && articleTitle[len(articleTitle)-1] == '/' {
				this.serveRedirect(w, this.opts.ExpectBaseURL+"view/"+url.PathEscape(articleTitle[0:len(articleTitle)-1]))
				return
			}

			this.serveNoSuchArticle(w, articleTitle)
			return
		}
		this.serveErrorMessage(w, err)
		return
	}

	if string(a.Body) == bbcodeIsDeleted {
		this.serveDeleted(w, articleTitle)
		return
	}

	pto := DefaultPageTemplateOptions(this.opts)
	pto.CurrentPageName = articleTitle
	pto.CurrentPageIsArticle = true

	bcr := this.GetBBCodeRenderer()
	pto.Content = bcr.RenderHTML(string(a.Body))
	pto.LoadCodeResources = bcr.CodePresent

	this.servePageResponse(w, r, pto)
	return
}
