package yatwiki

import (
	"database/sql"
	"fmt"
	"html/template"
	"net/http"
	"net/url"
)

func (this *WikiServer) routeModify(w http.ResponseWriter, r *http.Request, articleTitle string) {

	isNewArticle := false

	a, err := this.db.GetLatestVersion(articleTitle)
	if err != nil {

		if err == sql.ErrNoRows {
			// this is fine - we're creating it for the first time
			isNewArticle = true

		} else {
			this.serveErrorMessage(w, err)
			return
		}
	}

	var pageTitleHTML string
	var baseRev int64
	var existingBody string
	if isNewArticle {
		pageTitleHTML = `Creating new article`
		baseRev = 0
	} else {
		pageTitleHTML = `Editing article &quot;<a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`view/`+url.PathEscape(articleTitle)) + `">` + template.HTMLEscapeString(articleTitle) + `</a>&quot;`
		baseRev = a.ArticleID
		existingBody = string(a.Body)
	}

	content := `
<h2>` + pageTitleHTML + `</h2><br>

<form method="POST" action="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`save`) + `" class="editor" accept-charset="UTF-8" id="form-edit-page">
	<div class="frm">
		<label>
			Save as:
			<input type="hidden" name="baserev" value="` + fmt.Sprintf("%d", baseRev) + `" />
			<input type="text" name="pname" value="` + template.HTMLEscapeString(articleTitle) + `"/>
		</label>
		<input type="submit" value="Save &raquo;">
		| <a href="` + template.HTMLEscapeString(this.opts.ExpectBaseURL+`formatting`) + `" target="_blank">formatting&nbsp;help</a>
`
	if len(this.opts.ContentedServer) > 0 {
		content += `
<script type="text/javascript" src="` + this.opts.ContentedServer + `sdk.js"></script>
		| <a href="javascript:;" id="open-contented-uploader">upload...</a>
		<script type="text/javascript">
			document.getElementById("open-contented-uploader").addEventListener("click", function() {
				contented.init("#contentctr", function(items) {
					for (var i = 0; i < items.length; ++i) {
						$("#contentctr textarea").append(" " + contented.getPreviewURL(items[i]) + " ");
					}
				});
			});
		</script>
		`
	}

	content += `
		|
		<a href="javascript:;" id="delete-page">delete</a>
		<script type="text/javascript">
			document.getElementById("delete-page").addEventListener("click", function() {
				if (confirm('Are you sure you want to delete this page?\nThe history will be preserved.')) {
					document.getElementsByName("content")[0].value = '[delete]';
					document.getElementById("form-edit-page").submit();
				}
			});
		</script>
	</div>
	<div id="contentctr"><textarea name="content">` + template.HTMLEscapeString(existingBody) + `</textarea></div>
</form>
	`

	pto := DefaultPageTemplateOptions(this.opts)
	if isNewArticle {
		pto.CurrentPageName = "New article"
	} else {
		pto.CurrentPageName = articleTitle
		pto.CurrentPageIsArticle = true
	}
	pto.Content = template.HTML(content)
	this.servePageResponse(w, r, pto)
	return

}
